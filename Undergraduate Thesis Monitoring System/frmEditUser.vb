﻿Imports MySql.Data.MySqlClient
Imports System.Data
Public Class frmEditUser

    Dim conn As New MySqlConnection
    Dim myAdapter As MySqlDataAdapter
    Dim myDataSet As DataSet
    Private Sub cmdSave_Click(sender As System.Object, e As System.EventArgs) Handles cmdSave.Click
        Dim pass1 As String
        Dim pass2 As String

        Dim sqlquery As String
        Dim myData As MySqlDataReader
        Dim myCommand As New MySqlCommand()

        'this is the connection string to the database
        conn.ConnectionString = "server=localhost; user id=root; password=root;" _
            & "database=db_monitoringsystem"

        Dim emptyTextBoxes =
         From txt In Me.Controls.OfType(Of TextBox)()
         Where txt.Text.Length = 0
         Select txt.Name
        If emptyTextBoxes.Any Then
            MessageBox.Show("Please fill up all the details")
            Exit Sub
        End If

        Try
            conn.Open()

        Catch ex As Exception
            MessageBox.Show("Error Connecting to Database.")
        End Try


        pass1 = txtPass.Text.Trim
        pass2 = txtPass2.Text.Trim


        sqlquery = "INSERT INTO tbl_login (user,pass,FirstName,LastName) VALUES ('" + txtUser.Text _
                       + "','" + txtPass.Text + "','" + txtFName.Text + "','" + txtLName.Text + "');"
        sqlquery = "UPDATE tbl_login SET user='" + txtUser.Text + "', pass = '" + txtPass.Text _
            + "', FirstName ='" + txtFName.Text + "', LastName ='" + txtLName.Text + "'"


        '  UPDATE table_name SET field1=new-value1, field2=new-value2[WHERE Clause]

        myCommand.Connection = conn
        myCommand.CommandText = sqlquery


        'TO DO-OMARcheck all user name



        If pass1 <> pass2 Then
            MessageBox.Show("Password do not match")
            txtPass2.Text = ""
            txtPass2.Focus()
        Else
            Try
                'start query
                'myAdapter.SelectCommand = myCommand
                myData = myCommand.ExecuteReader()
                MessageBox.Show("New User created!")
                Me.Close()
                frmLogin.Show()

            Catch ex As Exception
                MessageBox.Show("Error inserting record")
            End Try

        End If


        'connection close
        conn.Close()
        conn.Dispose()
    End Sub

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click

        Dim result = MessageBox.Show("message", "caption", MessageBoxButtons.OKCancel
                                     )
        If result = DialogResult.Cancel Then
            Exit Sub
        ElseIf result = DialogResult.OK Then
            Me.Close()
            frmLogin.Show()
        End If


    End Sub
End Class
﻿Imports MySql.Data.MySqlClient
Imports System.Data

Public Class frmLogin

    Dim conn As MySqlConnection
    Public Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLogin.Click

        Dim myAdapter As New MySqlDataAdapter
        Dim sqlquery = "SELECT user, pass FROM tbl_login Where user='" & txtUser.Text _
                      & "' and pass='" & txtPass.Text & "'"

        Dim myCommand As New MySqlCommand()
        myCommand.Connection = conn
        myCommand.CommandText = sqlquery

        'connecting
        conn = New MySqlConnection()


        'this is the connection string to the database
        conn.ConnectionString = "server=localhost; user id=root; password=root;" _
            & "database=db_monitoringsystem"

        'test connection
        Try
            conn.Open()
        Catch ex As Exception
            MessageBox.Show("Error Connecting to Database.")
        End Try

        'start query
        myAdapter.SelectCommand = myCommand
        Dim myData As MySqlDataReader
        myData = myCommand.ExecuteReader()

        'see if user exits.
        If myData.HasRows = 0 Then
            MessageBox.Show("Invalid Login Details", "Login Error", _
                             MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtUser.Text = ""
            txtUser.Focus()
            txtPass.Text = ""


        Else
            Dim frm1 = New frmMain
            frm1.Show()
            Me.Visible = False
            Me.Hide()

        End If

        conn.Close()
        conn.Dispose()

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Application.Exit()

    End Sub

    Private Sub LogoPictureBox_Click(sender As System.Object, e As System.EventArgs) Handles LogoPictureBox.Click

    End Sub

    Private Sub frmLogin_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.AcceptButton = cmdLogin
        Me.CancelButton = cmdCancel

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        frmNewUser.Show()
        Me.Hide()

    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        frmEditLog.Show()
        Me.Hide()

    End Sub
End Class

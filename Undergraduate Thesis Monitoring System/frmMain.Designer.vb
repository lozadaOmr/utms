﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FILEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitProgramToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EDITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddNewThesisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditThesisToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteThesisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SEARCHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ABOUTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblUserDummy = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.EXITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ADDBOOKSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EDITTHESISToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ADDACCOUNTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EDITUSERACCOUNTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FILEToolStripMenuItem, Me.EDITToolStripMenuItem, Me.SEARCHToolStripMenuItem, Me.ABOUTToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(784, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FILEToolStripMenuItem
        '
        Me.FILEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogoutUserToolStripMenuItem, Me.ExitProgramToolStripMenuItem})
        Me.FILEToolStripMenuItem.Name = "FILEToolStripMenuItem"
        Me.FILEToolStripMenuItem.Size = New System.Drawing.Size(94, 20)
        Me.FILEToolStripMenuItem.Text = "APPLICATION"
        '
        'LogoutUserToolStripMenuItem
        '
        Me.LogoutUserToolStripMenuItem.Name = "LogoutUserToolStripMenuItem"
        Me.LogoutUserToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LogoutUserToolStripMenuItem.Text = "Log-out User"
        '
        'ExitProgramToolStripMenuItem
        '
        Me.ExitProgramToolStripMenuItem.Name = "ExitProgramToolStripMenuItem"
        Me.ExitProgramToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitProgramToolStripMenuItem.Text = "Exit Program"
        '
        'EDITToolStripMenuItem
        '
        Me.EDITToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddNewThesisToolStripMenuItem, Me.EditThesisToolStripMenuItem1, Me.DeleteThesisToolStripMenuItem})
        Me.EDITToolStripMenuItem.Name = "EDITToolStripMenuItem"
        Me.EDITToolStripMenuItem.Size = New System.Drawing.Size(43, 20)
        Me.EDITToolStripMenuItem.Text = "EDIT"
        '
        'AddNewThesisToolStripMenuItem
        '
        Me.AddNewThesisToolStripMenuItem.Name = "AddNewThesisToolStripMenuItem"
        Me.AddNewThesisToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.AddNewThesisToolStripMenuItem.Text = "Add New Thesis"
        '
        'EditThesisToolStripMenuItem1
        '
        Me.EditThesisToolStripMenuItem1.Name = "EditThesisToolStripMenuItem1"
        Me.EditThesisToolStripMenuItem1.Size = New System.Drawing.Size(159, 22)
        Me.EditThesisToolStripMenuItem1.Text = "Edit Thesis"
        '
        'DeleteThesisToolStripMenuItem
        '
        Me.DeleteThesisToolStripMenuItem.Name = "DeleteThesisToolStripMenuItem"
        Me.DeleteThesisToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.DeleteThesisToolStripMenuItem.Text = "Delete Thesis"
        '
        'SEARCHToolStripMenuItem
        '
        Me.SEARCHToolStripMenuItem.Name = "SEARCHToolStripMenuItem"
        Me.SEARCHToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.SEARCHToolStripMenuItem.Text = "SEARCH"
        '
        'ABOUTToolStripMenuItem
        '
        Me.ABOUTToolStripMenuItem.Name = "ABOUTToolStripMenuItem"
        Me.ABOUTToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.ABOUTToolStripMenuItem.Text = "ABOUT"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Logged in as"
        '
        'lblUserDummy
        '
        Me.lblUserDummy.AutoSize = True
        Me.lblUserDummy.Location = New System.Drawing.Point(96, 39)
        Me.lblUserDummy.Name = "lblUserDummy"
        Me.lblUserDummy.Size = New System.Drawing.Size(48, 13)
        Me.lblUserDummy.TabIndex = 2
        Me.lblUserDummy.Text = "DUMMY"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(35, 110)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&BORROW"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(134, 110)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "&RETURN"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(35, 156)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(708, 316)
        Me.DataGridView1.TabIndex = 5
        '
        'EXITToolStripMenuItem
        '
        Me.EXITToolStripMenuItem.Name = "EXITToolStripMenuItem"
        Me.EXITToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EXITToolStripMenuItem.Text = "EXIT"
        '
        'ADDBOOKSToolStripMenuItem
        '
        Me.ADDBOOKSToolStripMenuItem.Name = "ADDBOOKSToolStripMenuItem"
        Me.ADDBOOKSToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ADDBOOKSToolStripMenuItem.Text = "ADD"
        '
        'EDITTHESISToolStripMenuItem
        '
        Me.EDITTHESISToolStripMenuItem.Name = "EDITTHESISToolStripMenuItem"
        Me.EDITTHESISToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EDITTHESISToolStripMenuItem.Text = "EDIT"
        '
        'ADDACCOUNTToolStripMenuItem
        '
        Me.ADDACCOUNTToolStripMenuItem.Name = "ADDACCOUNTToolStripMenuItem"
        Me.ADDACCOUNTToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.ADDACCOUNTToolStripMenuItem.Text = "NEW USER ACCOUNT"
        '
        'EDITUSERACCOUNTToolStripMenuItem
        '
        Me.EDITUSERACCOUNTToolStripMenuItem.Name = "EDITUSERACCOUNTToolStripMenuItem"
        Me.EDITUSERACCOUNTToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.EDITUSERACCOUNTToolStripMenuItem.Text = "EDIT USER ACCOUNT"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblUserDummy)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "Undergraduate Monitoring System"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FILEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EXITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EDITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ADDBOOKSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EDITTHESISToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ADDACCOUNTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EDITUSERACCOUNTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SEARCHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ABOUTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblUserDummy As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents AddNewThesisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditThesisToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteThesisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitProgramToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
